# TLS, certificados de cliente, TOFU, e coisas similares

Este documento está em construção!

## Introdução

O Gemini obriga a que todas as conexões sejam protegidas por TLS. Ele também oferece suporte para o uso de certificados de cliente e esquemas alternativos de validação de certificados, um tema que não é muito familiar para muitas pessoas. Para muitos aspirantes a desenvolvedores Gemini, esta é a parte mais complicada. Daí que este documento vise fornecer uma introdução básica aos conceitos relevantes. Não sendo demasiado detalhado nem preciso, cumpre o objetivo de permitir que as pessoas entendam como tudo funciona, ainda que de forma abstrata, antes de quaisquer detalhes criptográficos.

Contudo, esta não é uma explicação a partir "do zero". Presume-se que você esteja já familiarizado com alguns conceitos básicos de criptografia assimétrica (ou de "chave pública"); não uma matemática que explique como tudo e porque tudo funciona, mas que explique o panorama conceitual. Basicamente, você deverá sentir-se à vontade com as seguintes ideias:

* Pares de chaves, ou seja, chaves públicas e privadas, cujo princípio de funcionamento faz com que tudo que seja criptografado com a primeira só possa ser descriptografado com a segunda
* Assinaturas digitais, ou seja, um método que permite que as pessoas possam usar a sua chave privada para "assinar" conteúdo digital, de forma a que outros possam validá-lo usando a sua chave pública, fazendo com que essas assinaturas não possam ser falsificadas. Dessa forma, o conteúdo é assinado e fica protegido contra modificação

## Noções básicas de certificados

### O que é um certificado TLS?

Para os nossos propósitos, pense-se um certificado TLS como sendo uma combinação dos seguintes componentes:

* Uma chave pública
* Alguns metadados sobre a parte que supostamente detem essa chave pública - o chamado "subject" (sujeito) do certificado
* Uma assinatura digital em todo o certificado
* Alguns metadados sobre a parte que assinou o certificado - o chamado "issuer" (emissor) do certificado
* Uma data de validade

Os metadados do subjet e do issuer possuem a mesma estrutura. O que há de sofisticado é que um certificado contém o "Distinguished Name" (DN) do subject e do issuer. Um DN possui vários campos, mas o mais importante é o chamado "Common Name" (CN). Não tendo que ser obrigatoriamente assim, o uso típico no mundo real dita que o CN do subject é quase sempre um nome de host, como `paypal.com`.

Num certificado há, geralmente, mais do que estas informações, mas, para já, esta informação é a suficiente para que se entenda o restante conteúdo deste artigo. A maior parte das vezes podemos esquecer a data de validade do certificado e apenas pensar no certificado como o conjunto de informações constituído por uma chave pública, o nome de alguém que afirma ser o seu proprietário, e o nome e a assinatura de alguém que certifica (daí o nome certificado!) essa reivindicação como sendo verdadeira.

### Como são tipicamente usados os certificados TLS?

Suponha que você é o admnistrador legítimo do servidor denominado `example.com`. A forma "normal" de usar o TLS é contactar um terceiro confiável, a que chamamos "Certificate Authority" (CA), e dizer "Olá, CA, o meu DN é bla, bla, bla e o meu CN é example.com. Este conjunto de bytes é a minha chave pública". Por seu lado, a CA "fará algo" para ter a certeza de que você é, de facto, o administrador legítimo de example.com, e então usará a sua chave privada para assinar um certificado que apresenta o seu DN (como subject),o DN da CA (como issuer) e mais uma data de validade. A partir desse momento, poderá configurar o seu servidor web ou o seu servidor de e-mail, ou qualquer outra aplicação que o necessite, para que seja usado o seu certificado sempre que os clientes necessitarem de TLS para funcionar.

Do lado do cliente, os navegadores da web e/ou os sistemas operativos vêm geralmente com uma grande pilha pré-instalada de chaves públicas para CAs reconhecidas. Quando uma pessoa se conecta ao seu servidor web e obtém o seu certificado, pode usar o DN do seu CA para encontrar a chave pública na sua pilha, de forma a poder validar a assinatura no seu certificado. Se a assinatura for válida, ela sabe que, de facto, aquela CA fidedigna está convencida de que a chave pública naquele certificado pertence realmente a example.com. Uma vez que o cliente confia na CA, a chave pública é validada como genuína e (assumindo que a data de validade não tenha caducado) está tudo pronto: o cliente e o servidor usam a chave pública autenticada para inicializar um canal seguro, através de um processo complicado que na verdade tem muito pouco a ver com o certificado.

### Isto tudo é necessário?

Usemos um caso prático. Suponha que um cliente se conecta a example.com e o servidor envia uma chave pública vazia em vez de um certificado. Ainda assim, o cliente pode usar essa chave para criptografar as informações enviadas para o servidor, e essas informações estariam protegidas de bisbilhoteiros. O risco é que alguém mal intencionado pode não apenas espionar a conexão, mas também interferir ativamente - como o ISP do cliente ou servidor, ou alguém que enganou o provedor de DNS do cliente para direcionar o domínio example.com para o seu próprio servidor -, agindo como "Man in the Middle" (MitM). Quando o cliente se tenta conectar ao servidor, o atacante envia a sua *própria* chave pública de volta ao cliente. Ao mesmo tempo, eles se conectam ao servidor pretendido, recebendo a chave pública real do servidor. O atacante usa a sua própria chave privada para descriptografar tudo o que obtém do cliente e, em seguida, criptografa novamente usando a chave pública do servidor, antes de enviá-la ao servidor - e vice-versa, na outra direção. O cliente e o servidor ainda podem comunicar entre si e tudo parece funcionar, mas o atacante pode ler tudo, mesmo que seja criptografado pelo cliente e pelo servidor.

Para evitar os problemas atrás descritos, o cliente precisa de saber que qualquer que seja a chave pública que seja enviada através do canal de comunicação ela pertence realmente a example.com, e não a algum atacante que intercetou a conexão e substituiu a chave. Não haja ilusões, pois este é um problema complicado. Existem muitas soluções possíveis, todas com diferentes prós e contras. A solução principal na Internet de hoje é usar certificados assinados por CAs. Basicamente, envolve terceirizar o difícil trabalho de garantir que quem diz possuir uma chave pública é realmente quem a possui, e quem está em melhores condições de o fazer é um pequeno número (em relação ao número de computadores na Internet) de partes confiáveis ​​(os CAs), que efetivamente fazem um bom trabalho.

Assim, se deixarmos de parte os detalhes técnicos contidos *num* certificado TLS e o que os computadores fazem com eles, e pensarmos apenas em termos da *função* que desempenham, os certificados TLS são uma forma de uma parte (issuer) garantir a validade do que é reclamado pela outra parte (a reclamação do subject de que "esta chave é minha") de uma forma fidedigna, que seja capaz de promover relações de confiança com quem precisa de acreditar nesse trabalho (por quem tem a chave pública do issuer e, desse modo, ser capaz de validar a sua assinatura digital).

### Ok! Então isso significa que com um sistema de CA temos o problema do MitM resolvido?

Ora bem, sim e não.

Há pessoas que não estão muito satisfeitas com a segurança oferecida pelo sistema CA. A menos que você empilhe um monte de extras em cima do procedimento de validação atrás descrito, *qualquer* CA cuja chave pública esteja pré-instalada no seu navegador ou sistema operativo pode assinar um certificado para *qualquer* nome de host, fazendo com que o seu sistema o aceite sem duvidar. Podemos estar a falar de uma lista de literalmente centenas de CAs, propriedade de empresas ou governos de todo o mundo, nas quais você pode depositar níveis diferenciados de confiança (realisticamente, você nunca ouviu falar ou interagiu anteriormente com a maioria deles). Se o seu sistema aceitar um certificado de qualquer CA, a segurança máxima do sistema será o equivalente à segurança oferecida pela CA menos confiável: a CA que faz a verificação menos cuidadosa das reivindicações de uma chave pública, ou a CA com a pior segurança da Internet, que obtém a sua chave privada roubada, ou a CA com os funcionários mais facilmente subornados, ou a CA cujo governo tem o poder para os obrigar a assinar um certificado falso, sob ameaça de prisão, ou pior. A CA mais fraca pode ser, de facto, muito fraca, e se se descobrir que uma certa CA "se tornou ela própria de pouca confiança", pode ser muito trabalhoso, e demorar muito tempo, remover a sua chave pública de cada dispositivo que a tem instalada.

Há pessoas menos preocupadas com a segurança fornecida pelo sistema CA e mais insatisfeitas por razões políticas ou filosóficas. O motivo é o facto de todo o sistema se basear num número relativamente pequeno de empresas privadas, com fins lucrativos, que administram uma infraestrutura centralizada cara e que cobra dinheiro aos utilizadores finais (às vezes muito dinheiro) para certificados.

### O que são certificados auto-emitidos e auto-assinados?

Um certificado é "auto-emitido" se o subject e o issuer forem a mesma entidade e "auto-assinado" se for assinado com uma chave privada correspondente à chave pública nele contida. Um certificado pode ser auto-emitido sem ser auto-assinado, e vice-versa, mas o caso mais comum (e aquele em que a maioria das pessoas pensa quando se fala em "certificados auto-assinados") é um certificado possuir ambas as propriedades.

Os certificados auto-assinados não se enquadram no uso típico de certificados: eles não funcionam como forma de proporcionar confiança quanto à propriedade de uma chave pública, porque não há terceiros envolvidos, como uma CA, na qual o cliente já confia e para a qual o servidor se autenticou. Qualquer pessoa pode emitir um certificado auto-assinado com qualquer CN de subject. Se você usar um certificado auto-assinado para o seu servidor, não há forma de um cliente o distinguir de um certificado que tenha sido emitido por um hacker, para um ataque MitM - pelo menos não apenas através da análise do certificado e da validação da assinatura. Esse é o motivo pelo qual os certificados auto-assinados são geralmente considerados inseguros.

Este não é um problema menor, mas importa entender que a diferença entre um certificado auto-assinado e um certificado assinado por uma CA é tão somente uma questão de saber se você pode garantir, apenas através da análise do próprio certificado, que a chave pública nele contida pertence realmente ao nome do host indicado. A chave pública contida num certificado auto-assinado não é diferente, nem inferior, à chave pública contida num certificado assinado por uma CA, e não existe nenhuma diferença na força da criptografia resultante do uso dessa chave. Isso significa que, em situações em que:

* você é capaz de validar que a chave realmente pertence ao nome do host, através de algum outro meio, ou
* você chega à conclusão que saber a quem pertence a chave não é realmente importante

os certificados auto-assinados são perfeitamente válidos para serem usados.

Neste momento, é provável que pergunte: qual é a vantagem em usar um certificado auto-assinado por oposição ao uso de apenas uma chave pública? Na verdade, a resposta é muito pouca vantagem. Há muitos protocolos seguros que usam chaves públicas vazias, sem certificados. Mas o TLS é construído com base no conceito de certificados assinados por CA e não possibilita a utilização de chaves públicas de forma isolada. Pensemos nos certificados auto-assinados como um hack para contornar esse constrangimento: eles permitem-nos usar a pilha de tecnologia TLS pré-existente, optando por prescindir do sistema de CA.

## Certificados de cliente

Brevemente.

## TOFU

Brevemente.

## Questões práticas

Brevemente.
