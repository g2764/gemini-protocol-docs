# Subscrição de páginas do Gemini

## Introdução

Trataremos agora de dar conta de uma convenção através da qual os clientes Gemini podem "subscrever" uma página Gemini que seja atualizada regularmente (como, por exemplo, a página de índice de um gemlog), sem que seja necessária a utilização de uma tecnologia de distribuição complexa como Atom ou RSS. Este método pretende ser uma alternativa menos pesada a essas tecnologias, de forma a reduzir as barreiras para a publicação de conteúdo em série no Geminispace, conteúdo esse que pode ser facilmente seguido sem a entediante consulta regular dos favoritos. Em particular, um dos objetivos explícitos é fazer com que uma página de índice simples, atualizada manualmente e legível por humanos, um tipo de conteúdo que os autores muito provavelmente criarão, possa ser subscrita sem que nenhuma alteração especial seja necessária. Obviamente, tal convenção será menos poderosa do que tecnologias mais complexas, como o Atom, e não funcionará tão bem em todos os casos possíveis de uso. No entanto, espera-se que funcione adequadamente para uma ampla gama de casos. Nada nesta convenção impede que os autores de conteúdo publiquem simultaneamente um feed Atom, se o desejarem. Na verdade, esta convenção pode até facilitar a geração desses feeds.

A informação que se segue descreve como interpretar um documento text/gemini como se de um feed Atom se tratasse, com todos os elementos necessários presentes. A convenção é descrita de forma a garantir que os clientes possam oferecer simultaneamente suporte a essa convenção e também à subscrição de feeds Atom. Para tal, usar-se-á uma base de código simplificada e uma interface de utilizador consistente, demonstrando-se como a geração automática simples de feeds Atom é possível. Os clientes mais simples, que suportem apenas a convenção de subscrição menos pesada, poderão ignorar os elementos Atom, se tal for adequado.

## Elementos de Feed 

O URL a partir do qual o documento text/gemini é obtido serve como elemento "id" obrigatório do feed e como elemento "link" recomendado.

O conteúdo da primeira linha do cabeçalho começada por um único # serve como o elemento "título" obrigatório do feed. Por esse motivo, os autores são encorajados a usar títulos que forneçam o seu próprio contexto, por ex. "Gemlog de Abelard Lindsay" em vez de "O meu gemlog" ou "Índice de Gemlog".

Se imediatamente após a primeira linha começada por um único # for encontrada uma linha de cabeçalho começada por ##, o seu conteúdo pode servir como elemento opcional de "subtítulo" do feed.

O elemento "atualizado" obrigatório de um feed deve conter o valor mais recente que for encontrado em todos os elementos "atualizados" obrigatórios da entrada associada. Se nenhuma entrada puder ser extraída do documento, então o feed ficará vazio (o que é permitido pelo padrão Atom) e o elemento "atualizado" do feed deve ser definido com a hora em que o documento foi obtido.

## Elementos de entrada

Os elementos de entrada de um feed derivam do subconjunto das suas linhas de link, se existir alguma.

As linhas de link em que o URL é seguido por uma label cujos primeiros 10 carateres correspondam a uma data no formato ISO 8601 (ou seja, AAAA-MM-DD) representam uma única entrada. As linhas de link que não atendam a esses critérios são ignoradas.

O elemento "id" obrigatório de uma entrada e o elemento "link" obrigatório com rel = "alternate" (geralmente, os elementos "link" são opcionais nas entradas Atom, mas esta convenção não assigna elementos "content" às entradas e, portanto, um link rel = alternate" torna-se obrigatório) são iguais ao URL da linha do link correspondente.

O elemento "atualizado" obrigatório de uma entrada deve ter como hora o meio-dia UTC do dia indicado pelo carimbo de data de 10 carateres no início da label da linha do link correspondente.

O elemento "título" obrigatório de uma entrada deriva do que resta da label da linha do link correspondente, após ser descartado o primeiro componente separado por espaços em branco (que necessariamente inclui o carimbo de data). Os clientes podem simplesmente considerar tudo o restante, mas uma higienização simples pode ser realizada para explicar o facto de que os utilizadores podem, por exemplo, usar labels com um separador entre a data e o título, como "23/03/1965 - lançamento do Gemini 3 bem-sucedido!".

## Exemplo

O seguinte documento Gemini, disponibilizado em gemini://gemini.jrandom.net/gemlog/:

```
# J. Random Geminaut's gemlog

Welcome to my Gemlog, where you can read every Friday about my adventures in urban gardening and abstract algebra!

## My posts

=> bokashi.gmi	2020-11-20 - Early Bokashi composting experiments
=> finite-simple-groups.gmi	2020-11-13 - Trying to get to grips with finite simple groups...
=> balcony.gmi	2020-11-06 - I started a balcony garden!

## Other gemlogs I enjoy

=> gemini://example.com/foo/	Abelard Lindsay's gemlog
=> gemini://example.net/bar/	Vladimir Harkonnen's gemlog
=> gemini://example.org/baz/	Case Pollard's gemlog
 
=> ../	Back to my homepage

Thanks for stopping by! 
```

pode ser interpretado como equivalente à estrutura do seguinte Atom feed:

```
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>J. Random Geminaut's gemlog</title>
  <link href="gemini://gemini.jrandom.net/gemlog/"/>
  <updated>2020-11-20T12:00:00Z</updated>
  <id>gemini://gemini.jrandom.net/gemlog/</id>

  <entry>
    <title>Early Bokashi composting experiments</title>
    <link rel="alternate" href="gemini://gemini.jrandom.net/gemlog/bokashmi.gmi"/>
    <id>gemini://gemini.jrandom.net/gemlog/bokashmi.gmi</id>
    <updated>2020-11-20T12:00:00Z</updated>
  </entry>

  <entry>
    <title>Trying to get to grips with finite simple groups...</title>
    <link rel="alternate" href="gemini://gemini.jrandom.net/gemlog/finite-simple-groups.gmi"/>
    <id>gemini://gemini.jrandom.net/gemlog/finite-simple-groups.gmi</id>
    <updated>2020-11-13T12:00:00Z</updated>
  </entry>

  <entry>
    <title>I started a balcony garden!</title>
    <link rel="alternate" href="gemini://gemini.jrandom.net/gemlog/balcony.gmi"/>
    <id>gemini://gemini.jrandom.net/gemlog/balcony.gmi</id>
    <updated>2020-11-06T12:00:00Z</updated>
  </entry>

</feed>
```

## Fragilidades

A principal lacuna desta convenção é o facto dela não indicar a hora do dia em que o post é escrito, nem o fuso horário do carimbo de data. Isso torna difícil que as aplicações façam um bom match quando estamos perante casos em que são esperadas várias atualizações num dia e em que a ordem relativa das atualizações (tanto dentro como fora da fontes do feed) é importante. Exemplos disso são os casos em que se pretende seguir as manchetes das últimas notícias, as atualizações meteorológicas, as informações do trânsito, etc. Nesses casos, essas aplicações são fortemente encorajadas a implementar tecnologias de subscrição mais robustas, como Atom ou RSS.

Não é expectável que esta fragilidade tenha implicações sérias para uma ampla gama de atividades comuns e valiosas no Geminispace, que operam à "escala humana". Por exemplo, esta convenção é perfeitamente viável para que um leitor individual possa usar o seu cliente local para subscrever dez ou vinte gemlogs escolhidos a dedo, que atualizem, com uma frequência não muito grande, conteúdo que não seja de caráter urgente ou imprescindível, como por exemplo um diário sobre o dia a dia das pessoas, hobbies, opiniões sobre o estado geral do mundo, receitas, fotos, etc. Este tipo de conteúdo não é de uma grande urgência, pois saber-se que foi escrito pela Alice na manhã de quarta-feira, ou foi escrito pelo Bob na noite de quarta-feira, ou saber-se exatamente quando cada pessoa escreveu os seus posts, não é relevante. Se a hora do dia for relevante para o conteúdo do post, o autor certamente fará questão de o mencionar expressamente.
