# Especificações complementares

As "especificações complementares" que apresentamos nesta página descrevem práticas opcionais que não fazem parte da especificação principal do protocolo Gemini, mas que se destinam a estabelecer diretrizes claras para uma "boa cidadania" no Geminispace e aumentar, através de convenções amplamente adotadas, a interoperabilidade do software Gemini.

=> robots.gmi robots.txt para Gemini
=> subscricao.gmi Subscrição de páginas Gemini
