# Buone pratiche per le implementazioni del protocollo Gemini

## Introduzione

Questo documento descrive varie convenzioni e piccoli consigli per implementare ed usare il protocollo Gemini che pur non facendo parte delle specifiche normanti, sono -in termini generali- considerate delle buone idee. Se state scrivendo software che implementa Gemini oppure state costruendo una capsula è consigliabile seguire i suggerimenti di seguito mostrati a meno che non riteniate vi siano valide motivazioni per non farlo.

## Nomi dei file

I server Gemini devono informare i client dei tipi MIME dei file che stanno servendo. La maniera più conveniente di dedurre il tipo MIME di un file è attraverso l'analisi della sua estensione. La mappatura [tra l'estensione del file e il tipo  MIME ndr] è ben standardizzata (e i sistemi unix possiedono spesso un file dedicato in "/etc/mime.types" contenente parecchie di queste associazioni); ma il problema rimane irrisolto rispetto a come i server dovrebbero riconoscere i file da servire con il tipo "text/gemini" che Gemini definisce.

I server attualmente in uso utilizzano come estensione dei file ".gmi" o ".gemini" proprio a tale scopo, i nuovi server sono invitati a mantenere questa convenzione invece che aggiungerne un'altra al cesto.

Mutuando le convenzioni dei server web, se viene proposta al server una richiesta che viene mappata in una directory sul filesystem e un file di nome "index.gmi" o "index.gemini" esiste in tale directory, allora il file viene servito a fronte di quella richiesta.

## Dimensione del file

I server Gemini non informano i client della dimensione dei file che forniscono, questo fatto può rendere difficile [da parte del client ndr] capire se una connessione sia stata chiusa prematuramente a causa di un errore del server. Il rischio che ciò accada aumenta proporzionalmente alla dimensione del file.

Inoltre Gemini non supporta la compressione di file di grandi dimensioni, o il supporto per checkusm che permettano di controllare l'integrità dei dati che si ricevono; Come prima, anche la possibilità di incorrere in corruzione dei dati aumenta con l'aumentare delle dimensioni del file.

Per tutte queste ragioni Gemini non è un protocollo molto adatto a trasferire file di "grosse dimensioni". La definizione esatta di "grosse dimensioni" dipende, in qualche misura, dalla velocità e dell'affidabilità della connessione ad internet e dalla pazienza degli utenti. Come regola di buon senso, file più grandi di 100 MiB potrebbero non essere adatti a transitare attraverso il protocollo Gemini.

Naturalmente, dato che Gemini supporta il collegamento ad altri contenuti on line tramite altri protocolli purché veicolabili tramite URL, è sempre possibile inserire link, in un documento Gemini, a file di grandi dimensioni serviti via HTTPS, BitTorrent, IPFS o quello che più stuzzica la vostra fantasia.

## Codifica del testo

Gemini supporta qualunque codifica del testo vogliate utilizzando il parametro "charset" dei tipi MIME "text/*". In questo modo è possibile servire contenuti testuali codificati con schemi desueti o che supportano particolari convenzioni regionali.

Per i contenuti nuovi, tuttavia, è fortemente consigliato usare semplicemente l'UTF-8. Le specifiche gemini obbligano i client a gestire tale codifica. Il supporto per altre codifiche è a discrezione del client e non è, quindi, garantito.

Servire il contenuto come UTF-8 massimizza sia l'accessibilità del contenuto stesso che l'utilità dei client più semplici che supportano solo l'UTF-8.

## Redirezioni

### Considerazioni generali

Le redirezioni sono state incluse nella definizione del protocollo Gemini primariamente per permettere la ristrutturazione o la migrazione di siti tra server senza rompere collegamenti esistenti. Un insieme di documenti ampio ed interconnesso senza tale utilizzo sarebbe inevitabilmente "fragile" (Nota: era "brittle", va bene tradotto così?)

Ad ogni modo le redirezioni hanno, generalmente, degli aspetti fastidiosi. Riducono la trasparenza di un protocollo e rendono più difficile alle persone la possibilità di operare scelte informate rispetto a quale link seguire e possono far filtrare informazioni sulle attività dell'utente a terze parti. In gemini le redirezioni non sono così terribili come nell'HTTP (dato che mancano i cookie, l'header "referrer" ecc.), ma sono considerate, nella migliore delle ipotesi, come un male necessario.

Per quanto detto, quindi, cercate di usare le redirezioni con parsimonia! Funzionalità come gli abbreviatori di indirizzo sono pressoché senza meriti. Come regola di buon senso, pensate a lungo e approfonditamente quando intendete usare le redirezioni per scopi diversi dall'evitare collegamenti interrotti.

### Limiti alle redirezioni

Alcuni client possono interrogare l'utente se intendono, o meno, seguire una redirezione. Altri possono anche decidere di seguire la redirezione senza chiedere una decisione all'utente. Se scrivete un client che segue le redirezioni automaticamente vi preghiamo di prendere in considerazioni questi problemi che potrebbero verificarsi.

Server Gemini mal configurati o con intenti malevoli potrebbero servire le redirezioni in una maniera tale da rinchiudere un client in un ciclo infinito di redirezioni. Client robusti dovranno essere abbastanza "intelligenti" da scoprire tali cicli e agire di conseguenza per romperlo. L'Implementazione più semplice di tale comportamento è che i client rifiutino di seguire più di N redirezioni consecutive. Si suggerisce che il numero N sia non più alto di 5: in linea con le raccomandazioni per l'HTTP (si veda l' RFC-2068).

### Redirezioni tra protocolli differenti

La redirezioni tra protocolli diversi (in inglese: "cross-protocol redirect) consiste nella redirezione da Gemini ad un protocollo differente (p.es. Gopher). Tale pratica è possibile con Gemini  ma è scoraggiata. Tuttavia server mal configurati o malevoli saranno sempre in grado di operare tali redirezioni, quindi client ben congegnati dovrebbero essere pronti a rispondere in maniera adeguata a tali tipi di comportamenti.

Si raccomanda, inoltre, che anche i client che seguono le redirezioni in maniera automatica avvertano l'utente e chiedano comunque conferma quando viene servita una redirezione a protocolli non cifrati con TLS, come HTTP o Gopher (assumendo che il client supporti tali protocolli). In questo modo si eviteranno trasferimenti, non intenzionali, in chiaro.

## Cifrari TLS

Il TLS 1.2 è permesso in Gemini a dispetto del fatto che la versione 1.3 sia drasticamente più semplice e -sempre quest'ultima- rimuova dalla prima molti sistemi di cifratura ritenuti fragili. Accettare il TLS 1.2 è una decisione dettata dal fatto che, attualmente, solo la libreria OpenSSL sembra avere un supporto buono per il TLS 1.3; quindi richiedere il TLS 1.3 come livello minimo di cifratura avrebbe scoraggiato l'uso di librerie come LibreSSL o BearSSL che hanno altri vantaggi rispetto a OpenSSL.

Gli autori o le autrici dei client e server che scelgono di supportare il TLS 1.2 dovrebbero -in linea di principio- permettere l'utilizzo di sistemi di cifratura che offrano livelli di sicurezza paragonabili al TLS 1.3. In particolare tali software dovrebbero:

* Usare solo sistemi sicuri di scambio delle chiavi Diffie-Hellman di tipo effimero (DHE) o effimero basate su curve ellittiche (ECDHE).
* Usare sistemi di cifratura dei dati come AES o ChaCha20.
* Usare SHA2 o SHA3 come famiglie di algoritmi di hashing per l'autenticazione dei dati.
